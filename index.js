const { splitJobs } = require('./scripts/SplitJobs');
const fs = require('fs');
const path = require('path');
const { getFilesNames } = require('./scripts/helper');

let reqPath =  path.join(__dirname, '/source/');
let exportPath = path.join(__dirname, '/exports');
let filesNames = getFilesNames(reqPath);

filesNames.forEach(fileName => {
    splitJobs(reqPath, exportPath, fileName);
})
