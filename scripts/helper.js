const fs = require('fs');

let getFilesNames = (path) => {
    let fileNames = []
    fs.readdirSync(path).forEach(function(file) {
        fileNames.push(file)
    });
    return fileNames;
}

let makeNewDir = (path) => {
    if (!fs.existsSync(path)) {
        fs.mkdir(path, (err) => { 
            if (err) { 
                return console.error("\x1b[31m", `Folder ${path} already exists, delete it`); 
            } 
            return console.log("\x1b[33m", `Directory created successfully: ${path}`); 
        }); 
    }
    else {
        return console.log('\x1b[33m', `Folder ${path} already exist, this operation will replace its contents`);
    }
}

let writeFile = (path, xmlObject) => {
    fs.writeFileSync(path, xmlObject, function(err, data) {
        if (err) {
          console.log(err);
        }
        else {

        }
    });
}


module.exports = {
    getFilesNames,
    makeNewDir,
    writeFile
}