'use strict'

const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path');
const { makeNewDir, writeFile, getFilesNames } = require('./helper');


let splitJobs = (reqPath, exportPath, fileName) => {
    let parser = new xml2js.Parser();
    let builder = new xml2js.Builder();
    let newFolderName = fileName.replace('.xml', '');
    let newFolderPath = (path.join(exportPath, newFolderName))
    let resourceFile = path.join(reqPath, fileName);
    let countOfJobObjects = 0;

    makeNewDir(newFolderPath)

    fs.readFile(resourceFile, (err, data) => {
        parser.parseString(data, (err, result) => {
            console.log('\x1b[0m', `Reading file ${resourceFile}`);
            console.log('\x1b[0m', `Starting creation of files...`)
            result.jobs.job.forEach((job) => {
                let xmlObject = {
                    jobs: {
                        '$': result.jobs['$'],
                        job: [job]
                    }     
                };        
                
                let xml = builder.buildObject(xmlObject);
                let exportFileName = job['$']['job-id'];
                let exportFilePath = path.join(exportPath, `${newFolderName}/${exportFileName}.xml`)
                
                writeFile(exportFilePath, xml);
                countOfJobObjects++
            })

            if (countOfJobObjects === getFilesNames(newFolderPath).length) {
                console.log('\x1b[32m', `${getFilesNames(newFolderPath).length} out of ${countOfJobObjects} job xml files were created`)
                console.log('\x1b[33m', `Check ${newFolderName} folder in exports folder`)
            }
            else {
                console.log(`Not all the files was created, try again`);
            }
        });
    })
}

module.exports = {
    splitJobs
}